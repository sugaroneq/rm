#ifndef DRZWI_H
#define DRZWI_H
#include <QString>
#include <QImage>


class Drzwi
{
public:
     Drzwi();
     ~Drzwi();
     virtual void dzial_po_dociagnieciu()=0; //boska interwencja
     virtual void dzial_po_zagraniu()=0;
      virtual void dzial_po_otwarciu_komnaty()=0;
      virtual void dziala_po_aktywowaniu()=0;

protected:
   int num_id;
   QString nazwa;
   QImage img;
   int typ;

};

#endif // DRZWI_H
