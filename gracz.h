# include <iostream>
# include <Qlist>
using namespace std;

class Gracz
{
    Gracz();
    ~Gracz();
public:
    int ID_gracz;
    string nazwa_gracza;
    int poziom;
    int zloto;
    Qlist karty_na_renku;
    Qlist karty_na_stole;

    void dodaj_gracza();
    void add_level();
    void dodaj_zloto(Skarb a);
    void dobranie_karty();

};
