#ifndef TALIA_H
#define TALIA_H
#include <QString>
#include <QImage>
#include<QList>
#include "drzwi.h"

class Talia: public QList<Drzwi*>
{
public:
    Talia();
    ~Talia();
    void tasuj();
};

#endif // TALIA_H
